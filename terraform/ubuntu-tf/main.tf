provider "aws" {
  shared_config_files      = ["$HOME/.aws/config"]
  shared_credentials_files = ["$HOME/.aws/credentials"]
  profile                  = "default"
}

resource "aws_instance" "InstanciaCloudDevopsTeam" {    
    ami = "ami-052efd3df9dad4825"
    instance_type = "t2.micro"
    key_name = "ec2_ubuntu_terraform_key"
    security_groups = ["${aws_security_group.InstanciaCloudDevopsTeam.name}"]
    root_block_device {
      delete_on_termination = true      
      volume_size = 30
      volume_type = "gp3"
    }
    tags = {
        Year = "2022"
        SO = "Ubuntu Server 22.04"
        Team = "Equipo3"
        Career = "Bootcamp DevOps"
        Institution = "Educacion IT"
        Proyect = "Carrera clouddevops"
        Name = "InstanciaCloudDevopsTeam-equipo3"
        Environment = "dev"
        }
  
}

resource "aws_security_group" "InstanciaCloudDevopsTeam" {
    name = "InstanciaCloudDevopsTeam-equipo3"
    description = "reglas para el security group"
    ingress {
        from_port = "22"
        to_port = "22"
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
  
}