# Notas sobre el despliegue en EKS

Para poder correr el plan/apply de este proyecto se debe crear otro archivo de terraform que contenga las credenciales de AWS

## terraform.tfvars

```
aws_access_key = "aca_tu_access_key"
aws_secret_key = "aca_tu_secret_key"

region                   = "us-east-1"
availability_zones_count = 2

project = "EKS-EduIT"

vpc_cidr         = "10.0.0.0/16"
subnet_cidr_bits = 8
```